import React from "react";
import { AiFillPhone } from "react-icons/ai";
import { FaFacebookF, FaTwitter } from "react-icons/fa";
import { IoMail } from "react-icons/io5";

export default function Footer() {
  return (
    <>
      <div
        className="row"
        style={{
          backgroundColor: "#11174C",
          padding: "30px",
        }}
      >
        <div className="col-lg-8  col-sm-12  text-white">
          <div>
            <div className="mt-2">
              <AiFillPhone style={{ fontSize: 20 }} />
              <span>&nbsp;&nbsp;&nbsp;&nbsp;+91 6380762728</span>
            </div>

            <div className="mt-2">
              <IoMail
                style={{
                  fontSize: 20,
                }}
              />
              <span>&nbsp;&nbsp;&nbsp;&nbsp;Fastservice@gmail.com</span>
            </div>

            <div className="mt-2">
              <FaFacebookF style={{ fontSize: 20 }} />

              <span>&nbsp;&nbsp;&nbsp;&nbsp;Fastservice</span>
            </div>
            <div className="mt-2">
              <FaTwitter style={{ fontSize: 20 }} />

              <span>&nbsp;&nbsp;&nbsp;&nbsp;Fastservice</span>
            </div>
          </div>
        </div>
        <div className="col-lg-4  col-sm-12  text-white">
          <form action="mailto:someone@example.com" enctype="text/plain">
            <div class="form-group mt-3">
              <textarea
                placeholder=" Enter your Feedback"
                class="form-control"
                id="exampleFormControlTextarea1"
                rows="3"
              ></textarea>
            </div>

            <button type="submit" class="btn btn-primary mt-2">
              Submit
            </button>
          </form>
        </div>
      </div>
    </>
  );
}
