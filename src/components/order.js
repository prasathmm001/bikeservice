import React, { useState } from "react";
import DataJson from "./array.json";
export default function Orders() {
  const [data, setData] = useState(DataJson);
  const render = data?.post?.map((post) => (
    <>
      <div className="mt-5">
        <div class="container-sm">
          <div class="card my-4">
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src={post.image} class="card-img h-100" />
              </div>
              <div class="col-md-8">
                <div class="card-body">
                  <div className="row">
                    <div className="col-lg-6 col-sm-12">
                      <table class="table">
                        <tbody>
                          <tr>
                            <th scope="col">Name </th>
                            <td>:</td>
                            <td>{post.name}</td>
                          </tr>
                          <tr>
                            <th scope="col">Location</th>
                            <td> :</td>
                            <td>{post.location}</td>
                          </tr>
                          <tr>
                            <th scope="col">Status</th>
                            <td>:</td>
                            <td className="text-danger"> {post.status}</td>
                          </tr>
                          <tr>
                            <th scope="col">On Ready</th>
                            <td>:</td>
                            <td className="text-danger"> {post.ready}</td>
                          </tr>

                          <tr>
                            <th scope="col">On Delivery</th>
                            <td>:</td>
                            <td className="text-danger"> {post.ondelivery}</td>
                          </tr>
                          <tr>
                            <th scope="col">Completed</th>
                            <td>:</td>
                            <td className="text-danger"> {post.completed}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div className="col-lg-6 col-sm-12">
                      <table class="table">
                        <tbody>
                          <tr>
                            <th scope="col">General Service </th>
                            <td>:</td>
                            <td>&#8377;{post.charge1}</td>
                          </tr>
                          <tr>
                            <th scope="col">Oil Service</th>
                            <td> :</td>
                            <td> &#8377;{post.charge2}</td>
                          </tr>
                          <tr>
                            <th scope="col">Water Service</th>
                            <td>:</td>
                            <td> &#8377;{post.charge3}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  ));

  return (
    <div>
      <br />
      <br />
      <br />
      <p className="display-6 text-center mt-3" style={{ fontWeight: "bold" }}>
        Your Order
      </p>
      {render}
    </div>
  );
}
