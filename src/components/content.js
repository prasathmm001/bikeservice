import React from "react";
import { Link, useHistory } from "react-router-dom";
import "./nav.css";
function Navigation() {
  let history = useHistory();
  return (
    <>
      <div className="container-fluid">
        <nav className="navbar navbar-expand-lg navbar-light">
          <div class="container-md">
            <a className="navbar-brand brand" href="/">
              <h4 className="mt-2 logo" style={{ color: "yellow" }}>
                F&nbsp;A&nbsp;S&nbsp;T&nbsp;S&nbsp;E&nbsp;R&nbsp;V&nbsp;I&nbsp;C&nbsp;E
              </h4>
            </a>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbar"
              aria-controls="navbar"
              aria-label="Toggle navigation"
            >
              <span class="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbar">
              <ul className="navbar-nav ms-auto">
                <li className="nav-item ms-4">
                  <Link
                    to="/signup"
                    className="nav-link text-center nav-links  text-white"
                    style={{ fontWeight: 460, fontSize: 20 }}
                  >
                    Home
                  </Link>
                </li>
                <li className="nav-item ms-4">
                  <Link
                    to="/service"
                    className="nav-link text-center nav-links text-white"
                    style={{ fontWeight: 460, fontSize: 20 }}
                  >
                    services
                  </Link>
                </li>

                <li className="nav-item ms-4">
                  <Link
                    to="/myprofile"
                    className="nav-link text-center nav-links text-center text-white"
                    style={{ fontWeight: 460, fontSize: 20 }}
                  >
                    Order
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </>
  );
}

export default Navigation;
