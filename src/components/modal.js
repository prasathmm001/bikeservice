import React, { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { AiFillCloseCircle } from "react-icons/ai";
import DataJson from "./array.json";
function Example(props) {
  const values = [true];
  const [fullscreen, setFullscreen] = useState(true);
  const [show, setShow] = useState(false);
  const [data, setData] = useState(DataJson);

  function handleShow(breakpoint) {
    setFullscreen(breakpoint);
    setShow(true);
  }
  const values1 = [true];
  const [fullscreen1, setFullscreen1] = useState(true);
  const [show1, setShow1] = useState(false);
  const [Genservice, setGenservice] = useState("");
  const [Oilservice, setOilservice] = useState("");
  const [name, setname] = useState("");
  const [mail, setmail] = useState("");
  const [phone, setphone] = useState("");
  const [street, setstreet] = useState("");
  const [city, setcity] = useState("");
  const [state, setstate] = useState("");
  const [zip, setzip] = useState("");
  const [date, setdate] = useState("");
  var today = new Date();
  var todaydate =
    today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate;
  //90.68
  const [Waterwash, setWaterwash] = useState("");
  //372.7
  const total = Math.floor(Genservice + Oilservice + Waterwash);
  const onGen = () => {
    console.log("checked");
    setGenservice(481.19);
  };
  const onOil = () => {
    console.log("checked");
    setOilservice(437.7);
  };
  const onWater = () => {
    console.log("checked");
    setWaterwash(90.68);
  };
  const onClear = () => {
    setWaterwash("");
    setOilservice("");
    setGenservice("");
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    var obj = {
      name: props.data.name,
      image: props.data.image,
      location: props.data.location,
      ownername: props.data.ownername,
      phoneno: props.data.phone,
      charge1: props.data.charge1,
      charge2: props.data.charge2,
      charge3: props.data.charge3,
      ready: "bending",
      ondelivery: "bending",
      completed: "not completed",
      status: "bending",
      experiance: props.data.experiance,
      servicestreet: props.data.servicestreet,
      servicecity: props.data.servicecity,
      servicestate: props.data.servicestate,
      servicezip: props.data.servicezip,
      servicemail: props.data.servicemail,
      cname: name,
      cmail: mail,
      cphone: phone,
      cstreet: street,
      ccity: city,
      cstate: street,
      ccountry: state,
      czip: zip,
      oilcharger: Oilservice,
      waterwash: Waterwash,
      generalservice: Genservice,
      name: props.data.name,
      image: props.data.image,
      bookingdate: date,
      dates: todaydate,
    };

    data.post.push(obj);
    setData({ ...data });
    console.log("success");
    alert("successfully submitted");
    setOilservice("");
    setGenservice("");
    setWaterwash("");
    setname("");
    setphone("");
    setstate("");
    setstreet("");
    setcity("");
    setzip("");
    setdate("");
    setShow(!show);
    //   axios
    //     .post(
    //       "https://61083b61d73c6400170d38c3.mockapi.io/Facebook/servicecenter",
    //       {
    //         name: props.data.name,
    //         image: props.data.image,
    //         location: props.data.location,
    //         ownername: props.data.ownername,
    //         phoneno: props.data.phone,
    //         charge1: props.data.charge1,
    //         charge2: props.data.charge2,
    //         charge3: props.data.charge3,
    //         bending: "",
    //         ready: "",
    //         ondelivery: "",
    //         completed: "",
    //         status: " ",
    //         experiance: props.data.experiance,
    //         servicestreet: props.data.servicestreet,
    //         servicecity: props.data.servicecity,
    //         servicestate: props.data.servicestate,
    //         servicezip: props.data.servicezip,
    //         servicemail: props.data.servicemail,
    //         cname: name,
    //         cmail: mail,
    //         cphone: phone,
    //         cstreet: street,
    //         ccity: city,
    //         cstate: street,
    //         ccountry: state,
    //         czip: zip,
    //         oilcharger: Oilservice,
    //         waterwash: Waterwash,
    //         generalservice: Genservice,
    //         name: props.data.name,
    //         image: props.data.image,
    //         bookingdate: date,
    //         dates: todaydate,
    //       }
    //     )
    //     .then((res) => {
    //       console.log("success");
    //       alert("successfully submitted");
    //       setOilservice("");
    //       setGenservice("");
    //       setWaterwash("");
    //       setname("");
    //       setphone("");
    //       setstate("");
    //       setstreet("");
    //       setcity("");
    //       setzip("");
    //       setdate("");
    //       setShow(!show);
    //     });
  };
  function handleShow1(breakpoint) {
    setFullscreen1(breakpoint);
    setShow1(true);
  }

  return (
    <>
      {values.map((v, idx) => (
        <Button
          key={idx}
          style={{
            backgroundColor: "#11174C",
            border: "none",
            outline: "none",
          }}
          onClick={() => handleShow(v)}
        >
          Visit Store
        </Button>
      ))}
      <Modal
        show={show}
        fullscreen={fullscreen}
        onHide={() => setShow(false)}
        backdrop="static"
      >
        <Modal.Header
          style={{
            backgroundColor: "#11174C",
            color: "white",
          }}
        >
          <Modal.Title>Booking : {props.data.name}</Modal.Title>

          <AiFillCloseCircle
            style={{
              color: "white",
              fontSize: "30px",
              float: "right",
              cursor: "pointer",
            }}
            onClick={(e) => {
              setShow(!show);
            }}
          />
        </Modal.Header>
        <Modal.Body style={{ backgroundColor: "#EBEDF5" }}>
          <div className="row p-3">
            <div className="col-md-6 col-xs-12">
              <img
                src={props.data.image}
                alt={props.data.name}
                className="img-fluid"
                width="100%"
              />

              <ul class="list-group">
                <li class="list-group-item">
                  <b>Name :</b>
                  <span> {props.data.name}</span>
                </li>
                <li class="list-group-item">
                  <b>Location :</b>
                  <span> {props.data.location}</span>
                </li>
                <li class="list-group-item">
                  <b>Manager Name :</b>
                  <span> {props.data.ownername}</span>
                </li>
                <li class="list-group-item">
                  <b>Phone :</b> <span> {props.data.phoneno}</span>
                </li>
                <li class="list-group-item">
                  <b>Address :</b>
                  <span>
                    {" "}
                    {props.data.servicestreet}, {props.data.servicecity},
                  </span>
                  {props.data.servicestate},{props.data.servicezip}
                </li>
              </ul>
            </div>
            <div className="col-md-6 col-xs-12">
              <h2 className="text-center">Available Services</h2>
              <div className="row">
                <div className="col-xs-12 mt-3  col-md-6 col-lg-4">
                  <div className="card">
                    <div className="card-body">
                      <img
                        src="https://content3.jdmagicbox.com/comp/def_content/motorcycle_repair_and_services-honda_authorised/default-motorcycle-repair-and-services-honda-authorised-5.jpg"
                        classNameName="card-img"
                        height="150px"
                        width="165px"
                        alt=""
                      />
                      <h5 className="card-title mt-3 text-center">
                        general Service
                      </h5>
                      <p className="card-text text-center">
                        <span>&#8377;&nbsp;&nbsp;{props.data.charge3}</span>
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-xs-12  mt-3 col-md-6 col-lg-4 ">
                  <div className="card">
                    <div class="card-body">
                      <img
                        src="https://www.denniskirk.com/blog/wp-content/uploads/2016/04/shutterstock_290597129.jpg"
                        className="card-img"
                        height="150px"
                        width="150px"
                        alt=""
                      />
                      <h5 class="card-title mt-3 text-center">Water Wash</h5>
                      <p class="card-text text-center">
                        <span>&#8377;&nbsp;&nbsp;{props.data.charge1}</span>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 mt-3 col-md-6 col-lg-4">
                  <div class="card">
                    <div class="card-body">
                      <img
                        src="https://cdn1.productnation.co/stg/sites/2/5e982e080a118"
                        className="card-img"
                        height="150px"
                        width="150px"
                        alt=""
                      />
                      <h5 class="card-title mt-3 text-center">Oil Service</h5>
                      <p class="card-text text-center">
                        <span>&#8377;&nbsp;&nbsp;{props.data.charge2}</span>
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-xs-12 col-md-6 col-lg-12">
                  <>
                    <center>
                      {values1.map((v, idx) => (
                        <Button
                          key={idx}
                          className="me-2 mt-3"
                          onClick={() => handleShow1(v)}
                          style={{
                            backgroundColor: "#11174C",
                            border: "none",
                            outline: "none",
                          }}
                        >
                          Book Service
                        </Button>
                      ))}
                    </center>
                    <Modal
                      show={show1}
                      fullscreen={fullscreen1}
                      onHide={() => setShow1(false)}
                    >
                      <Modal.Header
                        style={{
                          backgroundColor: "#11174C",
                          color: "white",
                        }}
                      >
                        <Modal.Title>
                          Booking service in : {props.data.name}
                        </Modal.Title>
                        <AiFillCloseCircle
                          style={{
                            color: "white",
                            fontSize: "30px",
                            float: "right",
                            cursor: "pointer",
                          }}
                          onClick={(e) => {
                            setShow1(!show1);
                          }}
                        />
                      </Modal.Header>
                      <Modal.Body style={{ backgroundColor: "#EBEDF5" }}>
                        <div className="row">
                          <div className="col-md-3 col-sm-0"></div>
                          <div className="col-md-5 col-sm-12">
                            <h5 className="text-center">
                              Enter Your Order Details
                            </h5>
                            <Form onSubmit={handleSubmit}>
                              <Form.Group className="mt-3">
                                <Form.Control
                                  required
                                  type="email"
                                  className="mt-3"
                                  placeholder="Email"
                                  onChange={(e) => {
                                    setmail(e.target.value);
                                  }}
                                />
                                <Form.Control
                                  required
                                  type="text"
                                  placeholder="Name"
                                  className="mt-3"
                                  onChange={(e) => {
                                    setname(e.target.value);
                                  }}
                                />
                                <Form.Control
                                  required
                                  type="text"
                                  placeholder="Phone"
                                  className="mt-3"
                                  onChange={(e) => {
                                    setphone(e.target.value);
                                  }}
                                />
                                <Form.Control
                                  required
                                  type="text"
                                  placeholder="DD/MM/YY"
                                  className="mt-3"
                                  onChange={(e) => {
                                    setdate(e.target.value);
                                  }}
                                />
                              </Form.Group>

                              <h5
                                className="ms-3 mt-3"
                                style={{ float: "right" }}
                              >
                                Total : {total}
                              </h5>
                              <h5 className="mt-3">Select Services</h5>
                              <Form.Group className="mb-3">
                                <Form.Label
                                  onClick={onGen}
                                  style={{ cursor: "pointer" }}
                                >
                                  General service
                                </Form.Label>
                                <br />
                                <Form.Label
                                  onClick={onOil}
                                  style={{ cursor: "pointer" }}
                                >
                                  Oil service
                                </Form.Label>
                                <br />
                                <Form.Label
                                  onClick={onWater}
                                  style={{ cursor: "pointer" }}
                                >
                                  Water Wash
                                </Form.Label>
                                <br />
                              </Form.Group>
                              <Button
                                className="btn-sm btn btn-danger"
                                onClick={onClear}
                              >
                                Clear All
                              </Button>
                              <Form.Group>
                                <Form.Control
                                  required
                                  type="text"
                                  placeholder="Street"
                                  className="mt-3"
                                  onChange={(e) => {
                                    setstreet(e.target.value);
                                  }}
                                />
                                <Form.Control
                                  required
                                  type="text"
                                  placeholder="City"
                                  className="mt-3"
                                  onChange={(e) => {
                                    setcity(e.target.value);
                                  }}
                                />
                                <Form.Control
                                  required
                                  type="text"
                                  placeholder="State"
                                  className="mt-3"
                                  onChange={(e) => {
                                    setstate(e.target.value);
                                  }}
                                />
                                <Form.Control
                                  required
                                  type="text"
                                  placeholder="Zip Code"
                                  className="mt-3"
                                  onChange={(e) => {
                                    setzip(e.target.value);
                                  }}
                                />
                              </Form.Group>
                              <center>
                                <Button
                                  className="btn  mt-2"
                                  type="submit"
                                  style={{
                                    backgroundColor: "#11174C",
                                    border: "none",
                                    outline: "none",
                                  }}
                                >
                                  Submit
                                </Button>
                              </center>
                            </Form>
                          </div>
                          <div className="col-md-3 col-sm-0"></div>
                        </div>
                      </Modal.Body>
                    </Modal>
                  </>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default Example;
//render(<Example />);
