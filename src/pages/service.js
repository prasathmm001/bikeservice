import axios from "axios";
import React, { useEffect, useState } from "react";
import Load from "../assets/loading.png";
import Footer from "../components/footer";
import Modals from "../components/modal";
import "./pages.css";
export default function Service() {
  const [center, setCenter] = useState([]);
  useEffect(() => {
    getAll();
  }, []);
  const getAll = () => {
    axios
      .get("https://61083b61d73c6400170d38c3.mockapi.io/Facebook/servicecenter")
      .then((res) => {
        setCenter(res.data);
        console.log(res.data);
      });
  };
  return (
    <>
      <div
        className="container-fluid"
        style={{ overflow: "hidden", backgroundColor: "#EBEDF5" }}
      >
        <h3
          className="display-5 text-center text-warning"
          style={{ fontWeight: "bold", color: "yellow" }}
        >
          Near By You
        </h3>
        {center?.length ? (
          center.map((datas) => (
            <div class="container-sm">
              <div class="card my-4">
                <div class="row no-gutters">
                  <div class="col-md-4">
                    <img src={datas.image} class="card-img h-100" />
                  </div>
                  <div class="col-md-8">
                    <div class="card-body">
                      <h5 class="card-title">{datas.name}</h5>

                      <div class="mb-5">
                        <p class="card-text float-right">
                          <small class="text-muted">
                            Experience : {datas.experiance} years
                          </small>
                        </p>
                        <p>Location : {datas.location}</p>
                      </div>

                      <Modals data={datas} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ))
        ) : (
          <>
            <center>
              <img
                src={Load}
                alt=""
                className="img-fluid"
                style={{ marginTop: "10%" }}
                height="100px"
                width="100px"
              />
            </center>
          </>
        )}
        <Footer />
      </div>
    </>
  );
}
