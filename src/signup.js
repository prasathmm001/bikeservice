import { useFormik } from "formik";
import React, { useContext, useState } from "react";
import { useHistory } from "react-router-dom";
import * as yup from "yup";
import "./components/nav.css";
import { AlertContext } from "./context/alerContext";

const Signin = (props) => {
  let history = useHistory();
  const { alert, setAlert } = useContext(AlertContext);
  const [hideden, setHidden] = useState("");
  const phoneRegExp =
    /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      password: "",
      phone: "",
    },
    validationSchema: yup.object({
      name: yup
        .string()
        .required("name is required")
        .min(3, "minimum 3 characters required"),
      email: yup.string().email().required("Email is required"),
      password: yup.string().required("passsword is required"),
      phone: yup
        .string()
        .required("Phone number is required")
        .matches(phoneRegExp, "Phone number is not valid"),
    }),
    onSubmit: (data) => {
      console.log(data);
      var axios = require("axios");
      var config = {
        method: "post",
        url: "https://mocker-collections-api.herokuapp.com/user/login",
        headers: {
          "Content-Type": "application/json",
        },
        data: data,
      };
      axios(config)
        .then(function (response) {
          console.log(JSON.stringify(response.data));
          setAlert("success");
          setHidden("alert alert-success");
        })
        .catch(function (error) {
          console.log(error);
          setAlert("error");
          setHidden("alert alert-danger");
          setTimeout(function () {
            setAlert("");
          }, 1000);
          var config = {
            method: "post",
            url: "https://mocker-collections-api.herokuapp.com/user/login",
            headers: {
              "Content-Type": "application/json",
            },
            data: data,
          };
        });
    },
  });

  return (
    <>
      <div className="container-sm mt-4 p-3">
        <div class="rounded p-3">
          <div class="center d-flex row ">
            <div className="col-3"></div>
            <div class="col-lg-6 col-sm-8 col-sm-10 mt-3">
              <div class={hideden} role="alert">
                {alert}
              </div>
              <div class="shadow-lg p-4">
                <h1 class="mt-3">Sign-up</h1>
                <form class="form" onSubmit={formik.handleSubmit}>
                  <input
                    type="text"
                    name="name"
                    class="w-100 input mt-3"
                    placeholder="Username"
                    style={{
                      padding: "13px",
                      borderRadius: "5px",
                      border: "1px solid grey",
                    }}
                    onChange={formik.handleChange}
                    value={formik.values.name}
                  />
                  {formik.errors.name ? (
                    <div class="text-danger">{formik.errors.name}</div>
                  ) : null}

                  <input
                    type="text"
                    name="email"
                    class="w-100 input mt-3"
                    placeholder=" Email"
                    style={{
                      padding: "13px",
                      borderRadius: "5px",
                      border: "1px solid grey",
                    }}
                    onChange={formik.handleChange}
                    value={formik.values.email}
                  />
                  {formik.errors.email ? (
                    <div class="text-danger">{formik.errors.email}</div>
                  ) : null}

                  <input
                    type="password"
                    name="password"
                    class="w-100 input mt-3"
                    placeholder="Password"
                    style={{
                      padding: "13px",
                      borderRadius: "5px",
                      border: "1px solid grey",
                    }}
                    onChange={formik.handleChange}
                    value={formik.values.password}
                  />
                  {formik.errors.password ? (
                    <div class="text-danger">{formik.errors.password}</div>
                  ) : null}

                  <input
                    type="text"
                    name="phone"
                    class="w-100 input mt-3"
                    placeholder="phone"
                    style={{
                      padding: "13px",
                      borderRadius: "5px",
                      border: "1px solid grey",
                    }}
                    onChange={formik.handleChange}
                    value={formik.values.phone}
                  />
                  {formik.errors.phone ? (
                    <div class="text-danger">{formik.errors.phone}</div>
                  ) : null}
                  <br />
                  <button
                    type="submit"
                    class="w-100 text-white  btn  mt-3"
                    style={{ padding: "13px", backgroundColor: "#382f9c" }}
                  >
                    <b> Create Account</b>
                  </button>
                  <br />
                  <button
                    class="w-100 text-white  btn btn-primary mt-3"
                    style={{ padding: "13px" }}
                    onClick={() => {
                      history.push("/register");
                    }}
                  >
                    Login
                  </button>
                  <br />
                </form>
              </div>
            </div>
            <div className="col-3"></div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Signin;
