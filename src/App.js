import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Navigation from "./components/content";
import Order from "./components/order";
import Home from "./pages/Home";
import Service from "./pages/service";
export default function App() {
  return (
    <Router>
      <Navigation />
      <Switch>
        <Route path="/signup" exact component={Home} />
        <Route path="/service" component={Service} />
        <Route path="/myprofile" component={Order} />
      </Switch>
    </Router>
  );
}
