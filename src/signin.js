import { useFormik } from "formik";
import React, { useContext, useState } from "react";
import { useHistory } from "react-router-dom";
import * as yup from "yup";
import "./components/nav.css";
import { AlertContext } from "./context/alerContext";

function Signin() {
  const { alert, setAlert } = useContext(AlertContext);
  const [hideden, setHidden] = useState("");
  let history = useHistory();
  const user =
    /^(?:[A-Z\d][A-Z\d_-]{5,10}|[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})$/i;
  const formik = useFormik({
    initialValues: {
      name: "",
      password: "",
    },
    validationSchema: yup.object({
      name: yup
        .string()
        .required("this field is required")
        .matches(user, "not valid"),
      password: yup
        .string()
        .required("passsword is required")
        .min(8, "minimum 8 characters required"),
    }),
    onSubmit: (data) => {
      history.push("/signup");
    },
  });
  return (
    <>
      <div className="container-sm mt-4 p-3">
        <div class="center rounded d-flex row ">
          <div className="col-md-3 col-sm-0"></div>
          <div class="col-lg-6 col-sm-8 col-sm-10 mt-3">
            <div class="shadow-lg p-4">
              <h1 class="mt-3">Sign-in</h1>
              <form class="form" onSubmit={formik.handleSubmit}>
                <input
                  type="text"
                  name="name"
                  class="w-100 input mt-3"
                  placeholder="Username or Email"
                  style={{
                    padding: "13px",
                    borderRadius: "5px",
                    border: "1px solid grey",
                  }}
                  onChange={formik.handleChange}
                  value={formik.values.name}
                />
                {formik.errors.name ? (
                  <div class="text-danger">{formik.errors.name}</div>
                ) : null}

                <input
                  type="password"
                  name="password"
                  class="w-100 input mt-3"
                  placeholder="Password"
                  style={{
                    padding: "13px",
                    borderRadius: "5px",
                    border: "1px solid grey",
                  }}
                  onChange={formik.handleChange}
                  value={formik.values.password}
                />
                {formik.errors.password ? (
                  <div class="text-danger">{formik.errors.password}</div>
                ) : null}

                <button
                  type="submit"
                  class="w-100 text-white  btn  mt-3"
                  style={{ padding: "13px", backgroundColor: "#382f9c" }}
                >
                  <b>Signin</b>
                </button>
                <br />
                <button
                  class="w-100 text-white  btn btn-primary mt-3"
                  style={{ padding: "13px" }}
                  onClick={() => {
                    history.push("/login");
                  }}
                >
                  Create an account
                </button>

                <br />
              </form>
            </div>
          </div>
          <div className="col-md-3 col-sm-0"></div>
        </div>
      </div>
    </>
  );
}

export default Signin;
