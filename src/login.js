import React, { useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Apps from "./App";
import { AlertContext } from "./context/alerContext";
import Signin from "./signin";
import Signup from "./signup";
export default function App() {
  const [alert, setAlert] = useState("");
  return (
    <Router>
      <Switch>
        <Route path="/signup" component={Apps} />
        <AlertContext.Provider value={{ alert, setAlert }}>
          <Route path="/" exact component={Signin} />
          <Route path="/login" component={Signup} />
          <Route path="/register" component={Signin} />
        </AlertContext.Provider>
      </Switch>
    </Router>
  );
}
